Download Neo4j [Community Edition](https://neo4j.com/download-center/#releases)
and startup the server
```
C:\Users\marc.donovan\neo4j-community-3.5.3\bin>neo4j console
2019-04-09 16:03:02.149+0000 INFO  ======== Neo4j 3.5.3 ========
2019-04-09 16:03:02.167+0000 INFO  Starting...
2019-04-09 16:03:05.757+0000 INFO  Bolt enabled on 127.0.0.1:7687.
2019-04-09 16:03:07.495+0000 INFO  Started.
2019-04-09 16:03:08.473+0000 INFO  Remote interface available at http://localhost:7474/
```

[Aviation Edge](https://aviation-edge.com/airline-routes-database-and-api/) is where I got the data. A business license is [$25 per month](https://aviation-edge.com/premium-api/) and you can make 500,000 calls per month for that price.

Aviation Edge key: 419a63-7c87d2 <-There are 100 free api calls for this free key (95 remain). So [get your own](https://aviation-edge.com/subscribe/signup.php?level=1) free key.

GET https://aviation-edge.com/v2/public/routes?key=419a63-7c87d2&airlineIata=BA <- This retrieves all British Airways routes (currently saved in file BA-flights.json file)

GET https://aviation-edge.com/v2/public/airportDatabase?key=419a63-7c87d2 <- This retrieves all airports (currently saved in file airports.json file)

There are three [node programs](https://nodejs.org/en/download/) here and you need to run parse airports first:
```javascript
node parse-airports.js
node parse-ba-flights.js
node parse-lh-flights.js
```

Once loaded, you can use the [Neo Java driver](https://neo4j.com/docs/driver-manual/current/) to run a Cypher query. This example finds all carriers that fly from Munich to Frankfort (currently only LH).
```
match (src:Airport {iata: 'MUC'})-[:FROM]-(od:OrigDest)-[:TO]-(dest:Airport {iata: 'FRA'}),(carriers:Carrier)-[:FLIES]-(od) return carriers
```
You can also run this [in the localhost browser](http://localhost:7474)

![alt text][screen]

[screen]: ./screen.png "Screenshot of browser"

To speed it up, create an index. The above query goes from 35ms to 2ms after applying an index.
```
CREATE INDEX ON :Airport(iata)
```

This deletes all nodes, so careful with this command
```
MATCH (n)
DETACH DELETE n
```


var fs = require("fs");
var neo4j = require('neo4j-driver').v1;
var driver = neo4j.driver("bolt://localhost", neo4j.auth.basic("neo4j", "neo4j"));
var session = driver.session();
var flights = JSON.parse(fs.readFileSync('LH-flights.json', 'utf-8'))

const iterate = async flights => {
  flights.forEach(flight => {
    // console.log(flight)
    // for each orig-dest pair, create an OrigDest record. MERGE creates the relationships. 
    // od, orig, dest, c are record instances. {} is how you inject data in the next section
    const query = `
    MATCH (orig:Airport { iata: {departIata} }),(dest:Airport { iata: {arriveIata} })
    MERGE (orig)-[:FROM]-(od:OrigDest)-[:TO]-(dest)
    MERGE (c:Carrier {iata:'LH'})-[:FLIES]-(od)
    RETURN c, od, orig, dest
    `
    session.run(query,
      {
        departTime: flight.departureTime,
        arriveTime: flight.arrivalTime,
        departIata: flight.departureIata,
        arriveIata: flight.arrivalIata,
        flightNumber: flight.flightNumber
      })
    .then(function (result) {
      // console.log(result)
    })
    .catch(function (error) {
      console.log(error)
    })
  })
}

const lh = async () => {
  await iterate(flights)
  session.close(() => {
    driver.close()
  })
}

lh()

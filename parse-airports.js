const fs = require("fs");
const neo4j = require('neo4j-driver').v1;
const driver = neo4j.driver("bolt://localhost", neo4j.auth.basic("neo4j", "neo4j"));
const session = driver.session();
const airports = JSON.parse(fs.readFileSync('airports.json', 'utf-8'))

// we only want airports that have IATA codes
const iatas = airports.filter(function(airport) {
  return airport.iataCode !== null &&
  airport.countryCode !== null &&
  airport.countryName != null &&
  airport.city !== null
})

const iterate = async iatas => {
  iatas.forEach(airport => {
    // merge will create a node if it does not exist yet
    session.run('MERGE (a:Airport {name: {nameParam}, iata: {iataParam} }) return a.name as name', 
      {nameParam: airport.name, iataParam: airport.iataCode})
    .then(function (result) {
      result.records.forEach(record => {
        console.log(record.get('name'))
      })
    })
    .catch(function (error) {
      console.log(error)
    })
  })
}

const runIatas = async () => {
  await iterate(iatas)
  session.close(() => {
    driver.close()
  })
}

runIatas()

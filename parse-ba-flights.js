const fs = require("fs");
const neo4j = require('neo4j-driver').v1;
const driver = neo4j.driver("bolt://localhost", neo4j.auth.basic("neo4j", "neo4j"));
const session = driver.session();
const flights = JSON.parse(fs.readFileSync('BA-flights.json', 'utf-8'))

const iterate = async flights => {
  flights.forEach(flight => {
    // console.log(flight)
    // for each orig-dest pair, create an OrigDest record. MERGE creates the relationships. 
    // od, orig, dest, c are record instances. {} is how you inject data in the next section
    const query = `
    MATCH (orig:Airport { iata: {departIata} }),(dest:Airport { iata: {arriveIata} })
    MERGE (orig)-[:FROM]-(od:OrigDest)-[:TO]-(dest)
    MERGE (c:Carrier {iata:'BA'})-[:FLIES]-(od)
    RETURN c, od, orig, dest
    `
    session.run(query,
    {
      departTime: flight.departureTime,
      arriveTime: flight.arrivalTime,
      departIata: flight.departureIata,
      arriveIata: flight.arrivalIata
    })
    .then(function (result) {
      // console.log(result)
    })
    .catch(function (error) {
      console.log(error)
    })
  })
}

const ba = async () => {
  await iterate(flights)
  session.close(() => {
    driver.close()
  })
}

ba()
